/* CS 367 -- Program 2 Observer Header
 *
 *   Dana Ballou
 * 	 CS 367
 */

#ifndef PROG2_OBSERVER_H
#define PROG2_OBSERVER_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

 #endif