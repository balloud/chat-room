/* CS 367 -- Program 1 Client 
 *
 *   Dana Ballou	
 * 	 CS 367
 */

/* Functions */


/* Headers */
#include "prog2_observer.h"

/* Main */
int main(int argc, char** argv) {

	int bufLen = 1028;	
	struct hostent *ptrh; /* pointer to a host table entry */
	struct protoent *ptrp; /* pointer to a protocol table entry */
	struct sockaddr_in sAddress; /* structure to hold an IP address */
	int sfd; /* socket descriptor */
	int port; /* protocol port number */
	char *host; /* pointer to host name */
	char buf[1028]; /* buffer for data from the server */
	memset((char *)&sAddress,0,sizeof(sAddress)); /* clear sockaddr structure */
	sAddress.sin_family = AF_INET; /* set family to Internet */

	if( argc != 3 ) {
		fprintf(stderr,"Error: Wrong number of arguments\n");
		fprintf(stderr,"usage:\n");
		fprintf(stderr,"./client server_address server_port\n");
		exit(EXIT_FAILURE);
	}
	port = atoi(argv[2]); /* convert to binary */
	fprintf(stderr, "%d\n", port);
	if (port > 0) /* test for legal value */
		sAddress.sin_port = htons((u_short)port);
	else {
		fprintf(stderr,"Error: bad port number %s\n",argv[2]);
		exit(EXIT_FAILURE);
	}
	host = argv[1]; /* if host argument specified */

	/* Convert host name to equivalent IP address and copy to sAddress. */
	ptrh = gethostbyname(host);
	if ( ptrh == NULL ) {
		fprintf(stderr,"Error: Invalid host: %s\n", host);
		exit(EXIT_FAILURE);
	}
	memcpy(&sAddress.sin_addr, ptrh->h_addr, ptrh->h_length); // not needed
	/* Map TCP transport protocol name to protocol number. */
	if ( ((long int)(ptrp = getprotobyname("tcp"))) == 0) {
		fprintf(stderr, "Error: Cannot map \"tcp\" to protocol number");
		exit(EXIT_FAILURE);
	}

	/* Create a socket. */
	sfd = socket(PF_INET, SOCK_STREAM, ptrp->p_proto);
	if (sfd < 0) {
		fprintf(stderr, "Error: Socket creation failed\n");
		exit(EXIT_FAILURE);
	}

	/* Connect the socket to the specified server. */
	if (connect(sfd, (struct sockaddr *)&sAddress, sizeof(sAddress)) < 0) {
		fprintf(stderr,"connect failed\n");
		perror("oops");
		exit(EXIT_FAILURE);
	}
	
	while (1) {
		memset(buf, 0, sizeof(buf));
		/*Read Loop*/
		if (recv(sfd, &buf, sizeof(buf), 0) < 0) {
			fprintf(stderr, "Client recieve failure.\n");
			exit(EXIT_FAILURE);
		}
		
		fprintf(stderr, buf);
	}
}