#------------------------
#Dana Ballou	

#------------------------

CC = gcc
CFLAGS= -g -Wall

hello: prog2_server.o prog2_participant.o prog2_observer.o
	gcc -g -o prog2_server prog2_server.o
	gcc -g -o prog2_participant prog2_participant.o
	gcc -g -o prog2_observer prog2_observer.o	
clean:
	rm -r prog2_server prog2_server.o prog2_participant prog2_participant.o prog2_observer prog2_observer.o
s:
	clear
	./prog2_server 12345 23456
p:  
	clear
	./prog2_participant localhost 12345
o:
	clear
	./prog2_observer localhost 23456

prog2_server.o prog2_participant.o prog2_observer.o: prog2_server.h prog2_participant.h prog2_observer.h
