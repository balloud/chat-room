/* CS 367 -- Program 2  Server
 *
 *   Dana Ballou
 * 	 CS 367
 */ 
/* Functions */

/* Headers */
#include "prog2_server.h"

/* Functions */
 char* prependUsr(int buf[1024], int userNum);

/* Main */
int main(int argc, char** argv) {

	struct protoent *ptrp; /* pointer to a protocol table entry */
	struct sockaddr_in pAddress; /* structure to hold server's address */
	struct sockaddr_in oAddress; /* structure to hold client's address */
	int participantSock, observerSock, newSock; /* socket file descriptors */
	int pPort, oPort; /* protocol port number */
	int adLength; /* length of address */
	fd_set sfdSet; /*Set of socket file descriptors */
	int highestSocket;
	int pArray[64];
	int oArray[64];
	int usernum;
	int maxSocks = 64;
	int buf[1024];
	int newBuf[1028];
	int activeConn;
	//Possible game types:
	if( argc != 3 ) {
		fprintf(stderr,"Error: Wrong number of arguments\n");
		fprintf(stderr,"usage:\n");
		fprintf(stderr,"./server participantPort observerPort\n");
		exit(EXIT_FAILURE);
	}	
	memset(pArray, 0, sizeof(pArray));
	memset(oArray, 0, sizeof(oArray));
	memset((char *)&pAddress,0,sizeof(pAddress)); /* clear sockaddr structure */
	memset((char *)&oAddress,0,sizeof(oAddress)); /* clear sockaddr structure */
	pAddress.sin_family = AF_INET; /* set family to Internet */
	pAddress.sin_addr.s_addr = INADDR_ANY; /* set the local IP address */
	oAddress.sin_family = AF_INET; /* set family to Internet */
	oAddress.sin_addr.s_addr = INADDR_ANY; /* set the local IP address */
	pPort = atoi(argv[1]); /* convert argument to binary */
	oPort = atoi(argv[2]); /* convert argument to binary */		

	if (pPort > 0) { /* fprintf(stderr, "Connected P\n");test for illegal value */
		pAddress.sin_port = htons((u_short)pPort);
	} else { /* print error message and exit */
		fprintf(stderr,"Error: Bad participant port number %s\n",argv[1]);
		exit(EXIT_FAILURE);
	}

	if (oPort > 0) { /* fprintf(stderr, "Connected P\n");test for illegal value */
		oAddress.sin_port = htons((u_short)oPort);
	} else { /* print error message and exit */
		fprintf(stderr,"Error: Bad observer port number %s\n",argv[2]);
		exit(EXIT_FAILURE);
	}

	/* Map TCP transport protocol name to protocol number */
	if ( ((long int)(ptrp = getprotobyname("tcp"))) == 0) {
		fprintf(stderr, "Error: Cannot map \"tcp\" to protocol number");
		exit(EXIT_FAILURE);
	}

	/* Create a sockets */	
	participantSock = socket(PF_INET, SOCK_STREAM, ptrp->p_proto);
	observerSock = socket(PF_INET, SOCK_STREAM, ptrp->p_proto);
	
	if (participantSock < 0) {
		fprintf(stderr, "Error: Socket creation failed\n");
		exit(EXIT_FAILURE);
	}
	if (observerSock < 0) {
		fprintf(stderr, "Error: Socket creation failed\n");
		exit(EXIT_FAILURE);
	}

	/* Bind a local address to the socket */
	if (bind(participantSock, (struct sockaddr *)&pAddress, sizeof(pAddress)) < 0) {
		fprintf(stderr,"Error: Bind failed\n");
		exit(EXIT_FAILURE);
	}
	if (bind(observerSock, (struct sockaddr *)&oAddress, sizeof(oAddress)) < 0) {
		fprintf(stderr,"Error: Bind failed\n");
		exit(EXIT_FAILURE);
	}

	/* Specify size of request queue */
	if (listen(participantSock, QLEN) < 0) {
		fprintf(stderr,"Error: Listen failed\n");
		exit(EXIT_FAILURE);
	}
	if (listen(observerSock, QLEN) < 0) {
		fprintf(stderr,"Error: Listen failed\n");
		exit(EXIT_FAILURE);
	}	

	/* Main server loop - accept and handle requests */
	
	while (1) {	

		FD_ZERO(&sfdSet); /* Clear set out */
		FD_SET(participantSock, &sfdSet);
		FD_SET(observerSock, &sfdSet);
		highestSocket = participantSock;
		if (participantSock >= observerSock) {
			highestSocket = participantSock;
		} else {
			highestSocket = observerSock;
		}
		int i = 0;
		while (i < maxSocks) {

			if (pArray[i] > 0) {
				FD_SET(pArray[i], &sfdSet);				
			}

			if (oArray[i] > 0) {
				FD_SET(oArray[i], &sfdSet);
			}

			if (pArray[i] > highestSocket) {
				highestSocket = pArray[i];
			}

			if (oArray[i] > highestSocket) {
				highestSocket = oArray[i];
			}
			i++;
		}
		
		/* Select function, look for active sockets */
		if (activeConn = select(highestSocket + 1 , &sfdSet , NULL , NULL , NULL) < 0) {
			fprintf(stderr, "Select Error.\n");
		}

		/* Look for connecting participants */
		if (FD_ISSET(participantSock, &sfdSet)) {
			/*Add new socket */
			if ((newSock = accept(participantSock, (struct sockaddr *)&pAddress, &adLength)) < 0) {
				fprintf(stderr, "Accept Failure\n");
				exit(EXIT_FAILURE);
			}
			/* get lowest available user number */			
			
			int j = 0;
			while (j < maxSocks) {

				if(pArray[j] == 0) {
					pArray[j] = newSock;					
					break;
				}
				j++;
			}
			if (j == (maxSocks)) {
				close(newSock);
			}
			if (j != (maxSocks)) {

				char newPrt[19];
				sprintf(newPrt, "User %d has joined\n", j);			

				int sendObvNP = 0;
				while (sendObvNP < maxSocks) {
					if (oArray[sendObvNP] > 0) {
						if (send(oArray[sendObvNP], &newPrt, sizeof(newPrt), 0) < 0) {
							fprintf(stderr, "Server send failure.\n");
							exit(EXIT_FAILURE);
						} 
					}
				sendObvNP++;
				}
			}
		}

		/* Read incoming messages and look for disconnecting participants */
		int k = 0;
		while (k < maxSocks) {			
			if (FD_ISSET(pArray[k], &sfdSet)) {
				

				int s = recv(pArray[k], &buf, sizeof(buf), 0);

				if (s <= 0) {
					
					close(pArray[k]);
					pArray[k] = 0;
					char dissconect[27];
					sprintf(dissconect, "User %d has left the room!\n", k);
					int sendDsct = 0;
					while (sendDsct < maxSocks) {
						if (oArray[sendDsct] > 0) {
							if (send(oArray[sendDsct], &dissconect, sizeof(dissconect), 0) < 0) {
								fprintf(stderr, "Server send failure.\n");
								exit(EXIT_FAILURE);
							} 
						}
					sendDsct++;
					}					
				} else {									
					int onesDigit;
					int tensDigit;
					/* Get correct size to send */					

					if (k < 10) {
						onesDigit = k;
						tensDigit = 0;
					} else {
						onesDigit = (k % 10);
						tensDigit = (k / 10);
					}
					
					int newBuf[1028];					
					sprintf(newBuf, "%d%d: %s", tensDigit, onesDigit, buf);					
					if ((s == 1024) && (buf[1023] != '\n')) {
						newBuf[1027] = '\n';
					}
					int size = (s + (sizeof(char) * 4));
					int sendIt = 0;
					while (sendIt < maxSocks) {
						if (oArray[sendIt] > 0) {
							if (send(oArray[sendIt], &newBuf, size, 0) < 0) {
								fprintf(stderr, "Server send failure.\n");
								exit(EXIT_FAILURE);
							} 
						}
					sendIt++;
					}
				}
			}
			k++;
		}
		/* Look for connecting observers */
		if (FD_ISSET(observerSock, &sfdSet)) {
			/*Add new Observer socket */
			if ((newSock = accept(observerSock, (struct sockaddr *)&oAddress, &adLength)) < 0) {
				fprintf(stderr, "Accept Failure\n");
				exit(EXIT_FAILURE);
			}

			int n = 0;
			while (n < maxSocks) {

				if(oArray[n] == 0) {
					oArray[n] = newSock;
					break;
				}
				n++;
			}
			if (n == (maxSocks)) {
				close(newSock);
			}		
			/* A new observer has joined. */	

			if (n != maxSocks) {
				char newObs[27];
				sprintf(newObs, "A new observer has joined!\n");
				
				int sendObv = 0;
				while (sendObv < maxSocks) {
					if (oArray[sendObv] > 0) {
						if (send(oArray[sendObv], &newObs, sizeof(newObs), 0) < 0) {
							fprintf(stderr, "Server send failure.\n");
							exit(EXIT_FAILURE);
						} 
					}
				sendObv++;
				}
			}
	    }
	    /* Look for disconnected observer */
	    int m = 0;
		while (m < maxSocks) {

			if (FD_ISSET(oArray[m], &sfdSet)) {	

				if (recv(oArray[m], &buf, sizeof(buf), 0) <= 0) {
					close(oArray[m]);
					oArray[m] = 0;					
				}				
			}
			m++;
		}
	}
	return 0;
}
