/* CS 367 -- Program 2 Server header *

 *   Dana Ballou	
 * 	 CS 367
 */

#ifndef PROG2_SERVER_H
#define PROG2_SERVER_H

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include "math.h"

#define QLEN 100

#endif